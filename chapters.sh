#!/bin/bash
#Extract each section page

missing() {
cat << EOF
The $1 executable is missing from your system.
Please install the program first, before using this script.
EOF
    exit 1
}

help() {
    cat << EOF
This script retrieves each chapter page. 
Written by Ben Cottrell.

Usage: $0 [file]
EOF
    exit 2
}

getChapters() {
    FILENAME=""
    LIST=""
    OUTPUT=$1
    
    mkdir _chapters
    
    for ((index=1; index < 12; index++)); do
            FILENAME="http://static-course-assets.s3.amazonaws.com/IntroNet50ENU/module$index/index.html"
            wget -q -O "_chapters/module"$index $FILENAME
            LIST=$(xmllint --html --xpath "//div[@id='nav']//a/@href" "_chapters/module"$index | sed 's/href\=//g' | tr -d \");
            for ITEM in $LIST;  do
                echo "http://static-course-assets.s3.amazonaws.com/IntroNet50ENU/module$index/"$ITEM >> $OUTPUT
            done;
            
            #echo "static-course-assets.s3.amazonaws.com/IntroNet50ENU/"$(xmllint --html --xpath "//div[@id='nav']//a/@href" "_chapters/module"$index | sed 's/href\=//g' | tr -d \") >> ./chapterList.txt
    done
}

if [ $# == 1 ]; then
    if [ ! $(which xmllint) ]; then
        missing "xmllint"
    fi
    
    if [ ! $(which wget) ]; then
        missing "wget"
    fi
    getChapters $1
else
    help
fi
