#!/bin/bash
#Append each section of the book from each retrieved webpage

function missing
{
    cat << EOF
    The $1 executable is missing from your system.
    Please install the program first, before using this script.
EOF
    exit 1
}

function help
{
    cat << EOF
This script compiles a book from the 'content' element found in multiple pages.    
Written by Ben Cottrell.

Usage: $0 [pages] [output]
EOF
    exit 2
}

function compile
{
    LIST=$(cat $1)
    FILENAME=""
    OUTPUT=$2
    
    cat << HEADER > $OUTPUT
<!DOCTYPE html>
<!-- Generated at $(date) -->
<html>
    <head>
        <meta charset="UTF-8"></meta>
        <title>Template</title>
        <link rel="stylesheet" href="default.css"/>
    </head>
    <body>
HEADER
    
    mkdir _pages
    
    for PAGE in $LIST; do
        echo $PAGE
        wget -q --directory-prefix="_pages/" $PAGE
        #Append the entry
        if [ $? == 0 ]; then
                #Extracts the title from a tag within a retrieved HTML document    
                FILENAME=$(basename $PAGE)
                cat << CONTENT>> $OUTPUT
                <h3>$(xmllint --html --xpath '/html/head/title/text()' "_pages/$FILENAME")</h3>
                <a href="$PAGE">Retrieved from $PAGE</a>
                <div class="section">
                $(xmllint --html --xpath "//div[@id='content']/node()" "_pages/$FILENAME")
                </div>
CONTENT
        else
               cat << SECTION >> $OUTPUT
                <h3>$(basename $PAGE)</h3>
                <div class="section">
                Unavailable
                </div>
SECTION
        fi
    done

    cat << FINAL >> $OUTPUT
    </body>
</html> 
FINAL
    
}


if [ $# == 2 ]; then
    #Check if the 'xmllint' executable and the files exist
    if [ ! $(which xmllint) ]; then
        missing "xmllint"
    fi
    
    if [ ! $(which wget) ]; then
        missing "wget"
    fi
     
    if [ -x $1 ]; then
        echo "A file containing a list of URL's is missing."
        exit 1
    fi
    compile $1 $2
else
    help
fi

